# SVG Circuit Symbols

A collection of various SVG circuit symbols drawn according to the IEC standard

## License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This <span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/StillImage" rel="dct:type">work</span> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

## Currently included

### Capacitor

- Capacitor
- Polarized capacitor
- Electrolytic capacitor

### Resistors

- Resistor
- Variable resistor
- Variable resistor (preset)
- Potentiometer